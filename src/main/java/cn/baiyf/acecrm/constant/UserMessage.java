package cn.baiyf.acecrm.constant;

public class UserMessage {

    public static final Integer USER_NOT_FOUND_CODE = 401;
    public static final String USER_NOT_FOUND_MSG = "用户名密码错误";

    public static final Integer LOGIN_SUCCESS_CODE = 200;
    public static final String LOGIN_SUCCESS_MSG = "登录成功";

    public static final String ADD_SUCCESS = "添加成功";
    public static final String EDIT_SUCCESS = "编辑成功";
    public static final String DELETE_SUCCESS = "删除成功";
}
