package cn.baiyf.acecrm.enums;

public enum IndustryEnum {

    IT(1, "IT"),
    FINANCE(2, "金融业"),
    REAL_ESTATE(3, "房地产"),
    BUSINESS_SERVICE(4, "商业服务"),
    EXPRESS(5, "运输/物流"),
    PRODUCT(6, "生产"),
    GOVERNMENT(7, "政府"),
    MEDIUM(8, "文化传媒");

    private final Integer code;

    private final String msg;

    IndustryEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return null;
    }

    public String getMsg() {
        return null;
    }
}
