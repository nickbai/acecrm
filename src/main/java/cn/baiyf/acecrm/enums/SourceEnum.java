package cn.baiyf.acecrm.enums;

public enum SourceEnum {

    PROMOTE(1, "促销"),
    SEARCH_ENGINE(2, "搜索引擎"),
    AD(3, "广告"),
    INTRODUCE(4, "转介绍"),
    ONLINE_REG(5, "线上注册"),
    ONLINE_ENQUIRY(6, "线上询价"),
    APPOINTMENT(7, "预约上门"),
    TEL_CONSULT(8, "电话咨询"),
    EMAIL_CONSULT(9, "邮件咨询");

    private final Integer code;

    private final String msg;

    SourceEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
