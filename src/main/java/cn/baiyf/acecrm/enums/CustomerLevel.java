package cn.baiyf.acecrm.enums;

public enum CustomerLevel {

    LEVEL_A(1, "A(重点客户)"),
    LEVEL_B(2, "B(普通客户)"),
    LEVEL_C(3, "C(非优先客户)");

    private Integer code;

    private String msg;

    CustomerLevel(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return null;
    }

    public String getMsg() {
        return null;
    }
}
