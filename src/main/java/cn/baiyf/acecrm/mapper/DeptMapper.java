package cn.baiyf.acecrm.mapper;

import cn.baiyf.acecrm.entity.DeptEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author NickBai
 * @since 2022-08-05
 */
public interface DeptMapper extends BaseMapper<DeptEntity> {

}
