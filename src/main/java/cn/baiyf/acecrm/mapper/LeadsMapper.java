package cn.baiyf.acecrm.mapper;

import cn.baiyf.acecrm.entity.LeadsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 线索表 Mapper 接口
 * </p>
 *
 * @author NickBai
 * @since 2022-08-10
 */
public interface LeadsMapper extends BaseMapper<LeadsEntity> {

}
