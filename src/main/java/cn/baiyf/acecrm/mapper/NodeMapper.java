package cn.baiyf.acecrm.mapper;

import cn.baiyf.acecrm.entity.NodeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 节点表 Mapper 接口
 * </p>
 *
 * @author NickBai
 * @since 2022-08-04
 */
public interface NodeMapper extends BaseMapper<NodeEntity> {

}
