package cn.baiyf.acecrm.mapper;

import cn.baiyf.acecrm.dto.UserListDto;
import cn.baiyf.acecrm.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author NickBai
 * @since 2022-08-03
 */
public interface UserMapper extends BaseMapper<UserEntity> {

    IPage<UserEntity> selectUserList(IPage<UserEntity> page, @Param("userListDto") UserListDto userListDto);
}
