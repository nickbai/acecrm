package cn.baiyf.acecrm.mapper;

import cn.baiyf.acecrm.entity.RoleEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author NickBai
 * @since 2022-08-05
 */
public interface RoleMapper extends BaseMapper<RoleEntity> {

}
