package cn.baiyf.acecrm.service;

import cn.baiyf.acecrm.dto.AddLeadsDto;
import cn.baiyf.acecrm.dto.LeadsDto;
import cn.baiyf.acecrm.entity.LeadsEntity;
import cn.baiyf.acecrm.util.Result;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 线索表 服务类
 * </p>
 *
 * @author NickBai
 * @since 2022-08-10
 */
public interface LeadsService extends IService<LeadsEntity> {

    Result getLeadsList(LeadsDto leadsDto);

    Result addLeads(AddLeadsDto addLeadsDto, String token);
}
