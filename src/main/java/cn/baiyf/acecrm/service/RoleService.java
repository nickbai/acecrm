package cn.baiyf.acecrm.service;

import cn.baiyf.acecrm.dto.RoleDto;
import cn.baiyf.acecrm.dto.StatusDto;
import cn.baiyf.acecrm.entity.RoleEntity;
import cn.baiyf.acecrm.util.Result;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author NickBai
 * @since 2022-08-05
 */
public interface RoleService extends IService<RoleEntity> {

    Result getRoleList(StatusDto statusDto);

    Result addRole(RoleDto roleDto);

    Result editRole(RoleDto roleDto);

    Result delRole(int roleId);
}
