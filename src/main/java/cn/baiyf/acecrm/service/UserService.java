package cn.baiyf.acecrm.service;

import cn.baiyf.acecrm.dto.LoginDto;
import cn.baiyf.acecrm.dto.UserAddDto;
import cn.baiyf.acecrm.dto.UserListDto;
import cn.baiyf.acecrm.entity.UserEntity;
import cn.baiyf.acecrm.util.Result;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author NickBai
 * @since 2022-08-03
 */
public interface UserService extends IService<UserEntity> {

    Result doLogin(LoginDto loginDto);

    Result getUserList(UserListDto userListDto);

    Result addUser(UserAddDto userAddDto);

    Result editUser(UserAddDto userAddDto);

    Result delUser(Integer userId);

    Result<List<UserEntity>> getUserListByRoleId(Integer roleId);

    Result<List<UserEntity>> getUserListByDeptId(Integer deptId);
}
