package cn.baiyf.acecrm.service;

import cn.baiyf.acecrm.dto.DeptDto;
import cn.baiyf.acecrm.dto.StatusDto;
import cn.baiyf.acecrm.entity.DeptEntity;
import cn.baiyf.acecrm.util.Result;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author NickBai
 * @since 2022-08-05
 */
public interface DeptService extends IService<DeptEntity> {

    Result getDeptTree(StatusDto statusDto);

    Result addDept(DeptDto deptDto);

    Result editDept(DeptDto deptDto);

    Result delDept(Integer id);
}
