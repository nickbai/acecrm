package cn.baiyf.acecrm.service;

import cn.baiyf.acecrm.entity.NodeEntity;
import cn.baiyf.acecrm.util.Result;
import com.baomidou.mybatisplus.extension.service.IService;

import java.lang.reflect.InvocationTargetException;

/**
 * <p>
 * 节点表 服务类
 * </p>
 *
 * @author NickBai
 * @since 2022-08-04
 */
public interface NodeService extends IService<NodeEntity> {

    Result getMenu(String token);

    Result showNode();
}
