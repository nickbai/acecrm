package cn.baiyf.acecrm.service.impl;

import cn.baiyf.acecrm.dto.MenuReDto;
import cn.baiyf.acecrm.entity.NodeEntity;
import cn.baiyf.acecrm.entity.TreeNodeEntity;
import cn.baiyf.acecrm.mapper.NodeMapper;
import cn.baiyf.acecrm.service.NodeService;
import cn.baiyf.acecrm.util.JwtUtil;
import cn.baiyf.acecrm.util.Result;
import cn.baiyf.acecrm.util.TreeTableUtil;
import com.auth0.jwt.interfaces.Claim;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 * 节点表 服务实现类
 * </p>
 *
 * @author NickBai
 * @since 2022-08-04
 */
@Service
public class NodeServiceImpl extends ServiceImpl<NodeMapper, NodeEntity> implements NodeService {

    @Autowired
    private MapperFacade mapperFacade;

    @Override
    public Result getMenu(String token) {

        Map<String, Claim> userInfo = JwtUtil.getPayloadByToken(token);
        int roleId = Integer.parseInt(userInfo.get("roleId").asString());

        // 非超级管理员
        if (roleId != 1) {
            return Result.success();
        }

        LambdaQueryWrapper<NodeEntity> nodeEntityQuery = new LambdaQueryWrapper<>();
        List<NodeEntity> nodeEntityList = baseMapper.selectList(nodeEntityQuery);
        List<String> menuMap = new ArrayList<>();
        nodeEntityList.forEach(item -> {
            if (item.getMenu() == 2 && item.getComponent() != null) {
                menuMap.add(item.getComponent());
            }
        });

        // 数据拷贝
        List<TreeNodeEntity> treeNodeList = mapperFacade.mapAsList(nodeEntityList, TreeNodeEntity.class);
        List<TreeNodeEntity> menuList = TreeTableUtil.list2TreeList(treeNodeList, "id", "nodePid", "children");

        MenuReDto menuReDto = new MenuReDto();
        menuReDto.setMenu(menuMap);
        menuReDto.setAuthMenu(menuList);
        menuReDto.setId(Integer.parseInt(userInfo.get("id").asString()));
        menuReDto.setRoleId(roleId);
        menuReDto.setName(userInfo.get("name").asString());
        menuReDto.setAvatar(userInfo.get("avatar").asString());

        return Result.success(menuReDto);
    }

    @Override
    public Result showNode() {

        LambdaQueryWrapper<NodeEntity> nodeEntityQuery = new LambdaQueryWrapper<>();
        List<NodeEntity> nodeEntityList = baseMapper.selectList(nodeEntityQuery);

        // 数据拷贝
        List<TreeNodeEntity> treeNodeList = mapperFacade.mapAsList(nodeEntityList, TreeNodeEntity.class);
        List<TreeNodeEntity> menuList = TreeTableUtil.list2TreeList(treeNodeList, "id", "nodePid", "children");

        return Result.success(menuList);
    }
}
