package cn.baiyf.acecrm.service.impl;

import cn.baiyf.acecrm.constant.UserMessage;
import cn.baiyf.acecrm.dto.PageReturnDto;
import cn.baiyf.acecrm.dto.RoleDto;
import cn.baiyf.acecrm.dto.StatusDto;
import cn.baiyf.acecrm.entity.RoleEntity;
import cn.baiyf.acecrm.mapper.RoleMapper;
import cn.baiyf.acecrm.service.RoleService;
import cn.baiyf.acecrm.util.Result;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author NickBai
 * @since 2022-08-05
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, RoleEntity> implements RoleService {

    @Override
    public Result getRoleList(StatusDto statusDto) {

        Page<RoleEntity> roleEntityPage = new Page<>(statusDto.getPage(), statusDto.getPageSize());
        LambdaQueryWrapper<RoleEntity> queryWrapper = new LambdaQueryWrapper<>();
        if (statusDto.getStatus() != null) {
            queryWrapper.eq(RoleEntity::getStatus, statusDto.getStatus());
        }

        if (!StringUtils.isEmpty(statusDto.getName())) {
            queryWrapper.like(RoleEntity::getName, statusDto.getName());
        }

        queryWrapper.orderByDesc(RoleEntity::getId);
        IPage<RoleEntity> roleEntityIPage = baseMapper.selectPage(roleEntityPage, queryWrapper);

        PageReturnDto<RoleEntity> pageReturn = new PageReturnDto<>();
        pageReturn.setData(roleEntityIPage.getRecords());
        pageReturn.setPages(roleEntityIPage.getPages());
        pageReturn.setTotal(roleEntityIPage.getTotal());
        pageReturn.setNowPage(roleEntityIPage.getCurrent());

        return Result.success(pageReturn);
    }

    @Override
    public Result addRole(RoleDto roleDto) {

        LambdaQueryWrapper<RoleEntity> query = new LambdaQueryWrapper<>();
        query.eq(RoleEntity::getName, roleDto.getName());
        RoleEntity roleInfo = baseMapper.selectOne(query);

        if (roleInfo != null) {
            return Result.success("该角色已经存在", "");
        }

        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setName(roleDto.getName());
        roleEntity.setRoleNode(StringUtils.join(roleDto.getRoleNode().toArray(), ","));
        roleEntity.setStatus(roleDto.getStatus());
        roleEntity.setCreateTime(DateUtil.now());
        baseMapper.insert(roleEntity);

        return Result.success(UserMessage.ADD_SUCCESS, "");
    }

    @Override
    public Result editRole(RoleDto roleDto) {

        LambdaQueryWrapper<RoleEntity> query = new LambdaQueryWrapper<>();
        query.eq(RoleEntity::getName, roleDto.getName());
        query.ne(RoleEntity::getId, roleDto.getId());
        RoleEntity roleInfo = baseMapper.selectOne(query);

        if (roleInfo != null) {
            return Result.success("该角色已经存在", "");
        }

        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setId(roleDto.getId());
        roleEntity.setName(roleDto.getName());
        roleEntity.setRoleNode(StringUtils.join(roleDto.getRoleNode().toArray(), ","));
        roleEntity.setStatus(roleDto.getStatus());
        roleEntity.setUpdateTime(DateUtil.now());
        baseMapper.updateById(roleEntity);

        return Result.success(UserMessage.EDIT_SUCCESS, "");
    }

    @Override
    public Result delRole(int roleId) {

        baseMapper.delete(new LambdaQueryWrapper<RoleEntity>().eq(RoleEntity::getId, roleId));

        return Result.success(UserMessage.DELETE_SUCCESS, "");
    }
}
