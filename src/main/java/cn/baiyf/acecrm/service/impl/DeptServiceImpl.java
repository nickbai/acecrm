package cn.baiyf.acecrm.service.impl;

import cn.baiyf.acecrm.constant.UserMessage;
import cn.baiyf.acecrm.dto.DeptDto;
import cn.baiyf.acecrm.dto.StatusDto;
import cn.baiyf.acecrm.entity.DeptEntity;
import cn.baiyf.acecrm.entity.TreeDeptEntity;
import cn.baiyf.acecrm.entity.TreeNodeEntity;
import cn.baiyf.acecrm.mapper.DeptMapper;
import cn.baiyf.acecrm.service.DeptService;
import cn.baiyf.acecrm.util.Result;
import cn.baiyf.acecrm.util.TreeTableUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @author NickBai
 * @since 2022-08-05
 */
@Service
public class DeptServiceImpl extends ServiceImpl<DeptMapper, DeptEntity> implements DeptService {

    @Autowired
    private MapperFacade mapperFacade;

    @Override
    public Result getDeptTree(StatusDto statusDto) {

        LambdaQueryWrapper<DeptEntity> deptListQuery = new LambdaQueryWrapper<>();
        if (statusDto.getStatus() != null) {
            deptListQuery.eq(DeptEntity::getStatus, statusDto.getStatus());
        }
        List<DeptEntity> deptList = baseMapper.selectList(deptListQuery);

        List<TreeDeptEntity> deptEntityList = mapperFacade.mapAsList(deptList, TreeDeptEntity.class);
        List<TreeDeptEntity> treeDeptEntityList = TreeTableUtil.list2TreeList(deptEntityList, "id", "pid", "children");
        // 排序
        treeDeptEntityList = treeDeptEntityList.stream().sorted(Comparator.comparing(DeptEntity::getId)).collect(Collectors.toList());

        return Result.success(treeDeptEntityList);
    }

    @Override
    public Result addDept(DeptDto deptDto) {

        DeptEntity deptInfo = baseMapper.selectOne(new LambdaQueryWrapper<DeptEntity>()
                .eq(DeptEntity::getName, deptDto.getName()));
        if (deptInfo != null) {
            return Result.fail("该部门已经存在", "");
        }

        DeptEntity deptEntity = new DeptEntity();
        deptEntity.setName(deptDto.getName());
        deptEntity.setStatus(deptDto.getStatus());
        deptEntity.setDescription(deptDto.getDesc());
        deptEntity.setPid(deptDto.getPid());
        deptEntity.setCreateTime(DateUtil.now());

        baseMapper.insert(deptEntity);

        return Result.success(UserMessage.ADD_SUCCESS, "");
    }

    @Override
    public Result editDept(DeptDto deptDto) {

        DeptEntity deptInfo = baseMapper.selectOne(new LambdaQueryWrapper<DeptEntity>()
                .eq(DeptEntity::getName, deptDto.getName())
                .ne(DeptEntity::getId, deptDto.getId()));
        if (deptInfo != null) {
            return Result.fail("该部门已经存在", "");
        }

        DeptEntity deptEntity = new DeptEntity();
        deptEntity.setId(deptDto.getId());
        deptEntity.setName(deptDto.getName());
        deptEntity.setStatus(deptDto.getStatus());
        deptEntity.setDescription(deptDto.getDesc());
        deptEntity.setPid(deptDto.getPid());
        deptEntity.setUpdateTime(DateUtil.now());

        baseMapper.updateById(deptEntity);

        return Result.success(UserMessage.EDIT_SUCCESS, "");
    }

    @Override
    public Result delDept(Integer id) {

        List<DeptEntity> deptList = baseMapper.selectList(new LambdaQueryWrapper<DeptEntity>().eq(DeptEntity::getPid, id));
        if (!CollectionUtils.isEmpty(deptList)) {
            return Result.fail("该部门下有子部门不可删除", "");
        }

        baseMapper.delete(new LambdaQueryWrapper<DeptEntity>().eq(DeptEntity::getId, id));

        return Result.success(UserMessage.DELETE_SUCCESS, "");
    }
}
