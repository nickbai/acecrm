package cn.baiyf.acecrm.service.impl;

import cn.baiyf.acecrm.constant.UserMessage;
import cn.baiyf.acecrm.dto.AddLeadsDto;
import cn.baiyf.acecrm.dto.LeadsDto;
import cn.baiyf.acecrm.dto.PageReturnDto;
import cn.baiyf.acecrm.entity.LeadsEntity;
import cn.baiyf.acecrm.entity.RoleEntity;
import cn.baiyf.acecrm.mapper.LeadsMapper;
import cn.baiyf.acecrm.service.LeadsService;
import cn.baiyf.acecrm.util.JwtUtil;
import cn.baiyf.acecrm.util.Result;
import cn.hutool.core.date.DateUtil;
import com.auth0.jwt.interfaces.Claim;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * <p>
 * 线索表 服务实现类
 * </p>
 *
 * @author NickBai
 * @since 2022-08-10
 */
@Service
public class LeadsServiceImpl extends ServiceImpl<LeadsMapper, LeadsEntity> implements LeadsService {

    @Override
    public Result getLeadsList(LeadsDto leadsDto) {

        Page<LeadsEntity> LeadsEntityPage = new Page<>(leadsDto.getPage(), leadsDto.getPageSize());
        LambdaQueryWrapper<LeadsEntity> queryWrapper = new LambdaQueryWrapper<>();
        if (!StringUtils.hasLength(leadsDto.getName())) {
            queryWrapper.eq(LeadsEntity::getName, leadsDto.getName());
        }

        if (!StringUtils.hasLength(leadsDto.getPhone())) {
            queryWrapper.eq(LeadsEntity::getPhone, leadsDto.getPhone());
        }

        if (!StringUtils.hasLength(leadsDto.getWechat())) {
            queryWrapper.eq(LeadsEntity::getWechat, leadsDto.getWechat());
        }

        if (!StringUtils.hasLength(leadsDto.getQq())) {
            queryWrapper.eq(LeadsEntity::getQq, leadsDto.getQq());
        }

        queryWrapper.orderByDesc(LeadsEntity::getId);
        IPage<LeadsEntity> leadsEntityIPage = baseMapper.selectPage(LeadsEntityPage, queryWrapper);

        PageReturnDto<LeadsEntity> pageReturn = new PageReturnDto<>();
        pageReturn.setData(leadsEntityIPage.getRecords());
        pageReturn.setPages(leadsEntityIPage.getPages());
        pageReturn.setTotal(leadsEntityIPage.getTotal());
        pageReturn.setNowPage(leadsEntityIPage.getCurrent());

        return Result.success(pageReturn);
    }

    @Override
    public Result addLeads(AddLeadsDto addLeadsDto, String token) {

        Result res = checkParam(addLeadsDto);
        if (res.getCode().equals(Result.FAIL_CODE)) {
            return res;
        }

        LeadsEntity leadsEntity = new LeadsEntity();
        leadsEntity.setName(addLeadsDto.getName());
        leadsEntity.setCompanyName(addLeadsDto.getCompanyName());
        leadsEntity.setSourceId(addLeadsDto.getSourceId());
        leadsEntity.setPhone(addLeadsDto.getPhone());
        leadsEntity.setTelphone(addLeadsDto.getTelphone());
        leadsEntity.setWechat(addLeadsDto.getWechat());
        leadsEntity.setQq(addLeadsDto.getQq());
        leadsEntity.setEmail(addLeadsDto.getEmail());
        leadsEntity.setAddress(addLeadsDto.getAddress());
        leadsEntity.setIndustry(addLeadsDto.getIndustry());
        leadsEntity.setLevel(addLeadsDto.getLevel());
        leadsEntity.setNextContactTime(addLeadsDto.getNextContactTime());
        leadsEntity.setRemark(addLeadsDto.getRemark());
        Map<String, Claim> userInfo = JwtUtil.getPayloadByToken(token);
        leadsEntity.setOwnerId(Integer.parseInt(userInfo.get("id").asString()));
        leadsEntity.setOwner(userInfo.get("name").asString());
        leadsEntity.setOperatorId(Integer.parseInt(userInfo.get("id").asString()));
        leadsEntity.setOperator(userInfo.get("name").asString());
        leadsEntity.setCreateTime(DateUtil.now());

        baseMapper.insert(leadsEntity);

        return Result.success(UserMessage.ADD_SUCCESS, "");
    }

    private Result checkParam(AddLeadsDto addLeadsDto) {
        if (!StringUtils.hasLength(addLeadsDto.getPhone()) &&
                !StringUtils.hasLength(addLeadsDto.getEmail()) &&
                !StringUtils.hasLength(addLeadsDto.getTelphone()) &&
                !StringUtils.hasLength(addLeadsDto.getWechat()) &&
                !StringUtils.hasLength(addLeadsDto.getQq())) {
            return Result.fail("手机、电话、邮箱、微信、qq必须填写一个", "");
        }

        if(!StringUtils.hasLength(addLeadsDto.getPhone()) &&
                !Pattern.matches("^1([38][0-9]|4[579]|5[0-3,5-9]|6[6]|7[0135678]|9[89])\\\\d{8}$",
                        addLeadsDto.getPhone())){
            return Result.fail("请输入正确的手机号", "");
        }

        if (!StringUtils.hasLength(addLeadsDto.getEmail()) &&
                !Pattern.matches("^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\\\.[a-zA-Z0-9_-]+)+$", addLeadsDto.getEmail())) {
            return Result.fail("请输入正确的邮箱", "");
        }

        // 带区号 ^[0][1-9]{2,3}-[0-9]{5,10}$
        // 不带区号 ^[1-9][0-9]{5,8}$
        if (!StringUtils.hasLength(addLeadsDto.getTelphone()) &&
                (!Pattern.matches("^[0][1-9]{2,3}-[0-9]{5,10}$", addLeadsDto.getPhone())) &&
                (!Pattern.matches("^[1-9][0-9]{5,8}$", addLeadsDto.getPhone()))) {
            return Result.fail("请输入正确的电话号", "");
        }

        return Result.success();
    }
}
