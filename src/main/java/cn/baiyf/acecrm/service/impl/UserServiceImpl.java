package cn.baiyf.acecrm.service.impl;

import cn.baiyf.acecrm.constant.UserMessage;
import cn.baiyf.acecrm.dto.*;
import cn.baiyf.acecrm.entity.UserEntity;
import cn.baiyf.acecrm.mapper.UserMapper;
import cn.baiyf.acecrm.service.UserService;
import cn.baiyf.acecrm.util.JwtUtil;
import cn.baiyf.acecrm.util.Result;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author NickBai
 * @since 2022-08-03
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements UserService {

    @Override
    public Result doLogin(LoginDto loginDto) {

        LambdaQueryWrapper<UserEntity> userQueryWrapper = new LambdaQueryWrapper<>();
        userQueryWrapper.eq(UserEntity::getName, loginDto.getName());
        userQueryWrapper.eq(UserEntity::getStatus, 1);

        UserEntity userInfo = baseMapper.selectOne(userQueryWrapper);
        if (userInfo == null) {
            return Result.fail(UserMessage.USER_NOT_FOUND_CODE, UserMessage.USER_NOT_FOUND_MSG, "");
        }

        String md5Str = loginDto.getPassword() + userInfo.getSalt();
        if (!SecureUtil.md5(md5Str).equals(userInfo.getPassword())) {
            return Result.fail(UserMessage.USER_NOT_FOUND_CODE, UserMessage.USER_NOT_FOUND_MSG, "");
        }

        Map<String, String> tokenMap = new HashMap<>();
        tokenMap.put("id", String.valueOf(userInfo.getId()));
        tokenMap.put("name", userInfo.getName());
        tokenMap.put("nickname", userInfo.getNickname());
        tokenMap.put("avatar", userInfo.getAvatar());
        tokenMap.put("roleId", String.valueOf(userInfo.getRoleId()));

        String token = JwtUtil.generateToken(tokenMap);

        LoginReDto loginReDto = new LoginReDto();
        loginReDto.setToken(token);
        loginReDto.setRoleId(userInfo.getRoleId());
        loginReDto.setUserId(userInfo.getId());

        return Result.success(UserMessage.LOGIN_SUCCESS_CODE, UserMessage.LOGIN_SUCCESS_MSG, loginReDto);
    }

    @Override
    public Result getUserList(UserListDto userListDto) {

        Page<UserEntity> userEntityPage = new Page<>(userListDto.getPage(), userListDto.getPageSize());
        IPage<UserEntity> userEntityIPage = baseMapper.selectUserList(userEntityPage, userListDto);

        PageReturnDto<UserEntity> pageReturn = new PageReturnDto<>();
        pageReturn.setData(userEntityIPage.getRecords());
        pageReturn.setPages(userEntityIPage.getPages());
        pageReturn.setTotal(userEntityIPage.getTotal());
        pageReturn.setNowPage(userEntityIPage.getCurrent());

        return Result.success(pageReturn);
    }

    @Override
    public Result addUser(UserAddDto userAddDto) {

        if (StringUtils.isEmpty(userAddDto.getPassword())) {
            return Result.fail(401, "请输入密码");
        }

        // 检测唯一
        LambdaQueryWrapper<UserEntity> query = new LambdaQueryWrapper<>();
        query.eq(UserEntity::getName, userAddDto.getName());
        UserEntity userInfo = baseMapper.selectOne(query);
        if (userInfo != null) {
            return Result.fail(400, "该用户已经存在");
        }

        UserEntity userEntity = new UserEntity();
        userEntity.setName(userAddDto.getName());
        userEntity.setNickname(userAddDto.getNickname());
        String salt = IdUtil.objectId();
        String md5Str = userAddDto.getPassword() + salt;
        userEntity.setPassword(SecureUtil.md5(md5Str));
        userEntity.setSalt(salt);
        userEntity.setAvatar(userAddDto.getAvatar());
        userEntity.setDeptId(userAddDto.getDeptId());
        userEntity.setRoleId(userAddDto.getRoleId());
        userEntity.setStatus(userAddDto.getStatus());
        userEntity.setIsLeader(userAddDto.getIsLeader());
        userEntity.setLeaderId(userAddDto.getLeaderId());
        userEntity.setCreateTime(DateUtil.now());
        baseMapper.insert(userEntity);

        return Result.success(UserMessage.ADD_SUCCESS, "");
    }

    @Override
    public Result editUser(UserAddDto userAddDto) {

        UserEntity userEntity = new UserEntity();

        // 检测唯一
        LambdaQueryWrapper<UserEntity> query = new LambdaQueryWrapper<>();
        query.eq(UserEntity::getName, userAddDto.getName());
        query.ne(UserEntity::getId, userAddDto.getId());
        UserEntity userInfo = baseMapper.selectOne(query);
        if (userInfo != null) {
            return Result.fail(400, "该用户已经存在");
        }

        userEntity.setName(userAddDto.getName());
        userEntity.setNickname(userAddDto.getNickname());

        if (!StringUtils.isEmpty(userAddDto.getPassword())) {
            String salt = IdUtil.objectId();
            String md5Str = userAddDto.getPassword() + salt;
            userEntity.setPassword(SecureUtil.md5(md5Str));
            userEntity.setSalt(salt);
        }

        userEntity.setId(userAddDto.getId());
        userEntity.setAvatar(userAddDto.getAvatar());
        userEntity.setDeptId(userAddDto.getDeptId());
        userEntity.setRoleId(userAddDto.getRoleId());
        userEntity.setStatus(userAddDto.getStatus());
        userEntity.setIsLeader(userAddDto.getIsLeader());
        userEntity.setLeaderId(userAddDto.getLeaderId());
        userEntity.setUpdateTime(DateUtil.now());
        baseMapper.updateById(userEntity);

        return Result.success(UserMessage.EDIT_SUCCESS, "");
    }

    @Override
    public Result delUser(Integer userId) {

        baseMapper.delete(new LambdaQueryWrapper<UserEntity>().eq(UserEntity::getId, userId));

        return Result.success(UserMessage.DELETE_SUCCESS, "");
    }

    @Override
    public Result<List<UserEntity>> getUserListByRoleId(Integer roleId) {
        return Result.success(baseMapper.selectList(new LambdaQueryWrapper<UserEntity>()
                .eq(UserEntity::getRoleId, roleId)));
    }

    @Override
    public Result<List<UserEntity>> getUserListByDeptId(Integer deptId) {
        return Result.success(baseMapper.selectList(new LambdaQueryWrapper<UserEntity>()
                .eq(UserEntity::getDeptId, deptId)));
    }
}
