package cn.baiyf.acecrm.config;

import cn.baiyf.acecrm.enums.ExceptionEnum;
import cn.baiyf.acecrm.exception.BusinessException;
import cn.baiyf.acecrm.util.ErrorUtil;
import cn.baiyf.acecrm.util.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

@Slf4j
@RestControllerAdvice
public class ExceptionHandlerConfig {

    /**
     * 业务异常处理
     *
     * @param e 业务异常
     * @return
     */
    @ExceptionHandler(value = BusinessException.class)
    @ResponseBody
    public Result exceptionHandler(BusinessException e) {
        log.error(ErrorUtil.errorInfoToString(e));
        return Result.fail(Integer.valueOf(e.getCode()), e.getErrorMsg());
    }

    /**
     * 空指针异常
     */
    @ExceptionHandler(value = NullPointerException.class)
    @ResponseBody
    public Result exceptionHandler(NullPointerException e) {
        log.error(ErrorUtil.errorInfoToString(e));
        return Result.fail(Integer.valueOf(ExceptionEnum.INTERNAL_SERVER_ERROR.getCode()),
                ExceptionEnum.INTERNAL_SERVER_ERROR.getMsg());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    private Result handleIllegalArgumentException(MethodArgumentNotValidException e) {
        BindingResult result = e.getBindingResult();
        String message = "";
        if (result.hasErrors()) {
            List<ObjectError> errors = result.getAllErrors();
            if (errors != null) {
                errors.forEach(p -> {
                    FieldError fieldError = (FieldError) p;
                    log.error("Data check failure : object{" + fieldError.getObjectName() + "},field{" + fieldError.getField() +
                            "},errorMessage{" + fieldError.getDefaultMessage() + "}");

                });
                if (errors.size() > 0) {
                    FieldError fieldError = (FieldError) errors.get(0);
                    message = fieldError.getDefaultMessage();
                }
            }
        }

        return Result.fail(Integer.valueOf(ExceptionEnum.BAD_REQUEST.getCode()), message);
    }

    /**
     * 未知异常处理
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result exceptionHandler(Exception e) {
        // 把错误信息输入到日志中
        log.error(ErrorUtil.errorInfoToString(e));
        return Result.fail(Integer.valueOf(ExceptionEnum.UNKNOWN.getCode()), ExceptionEnum.UNKNOWN.getMsg());
    }
}
