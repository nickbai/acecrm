package cn.baiyf.acecrm.dto;

import lombok.Data;

@Data
public class LeadsDto extends PageDto {

    private Integer type;

    private String name;

    private String phone;

    private String wechat;

    private String qq;
}
