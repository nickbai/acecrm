package cn.baiyf.acecrm.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class UserAddDto {

    private Integer id;

    @NotEmpty(message = "请输入登录账号")
    private String name;

    @NotEmpty(message = "请上传头像")
    private String avatar;

    private String nickname;

    @NotNull(message = "请选择所属角色")
    private Integer roleId;

    private Integer deptId;

    private String password;

    @NotNull(message = "请选择状态")
    private Integer status;

    private Integer isLeader;

    private Integer leaderId;
}
