package cn.baiyf.acecrm.dto;

import cn.baiyf.acecrm.entity.TreeNodeEntity;
import lombok.Data;
import java.util.List;

@Data
public class MenuReDto {

    private List<String> menu;

    private List<TreeNodeEntity> authMenu;

    private Integer id;

    private Integer roleId;

    private String name;

    private String avatar;
}
