package cn.baiyf.acecrm.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LoginDto {

    @NotNull(message = "请输入登录名")
    private String name;

    @NotNull(message = "请输入密码")
    private String password;
}
