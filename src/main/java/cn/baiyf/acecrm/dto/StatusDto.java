package cn.baiyf.acecrm.dto;

import lombok.Data;

@Data
public class StatusDto extends PageDto {

    private Integer status;

    private String name;
}
