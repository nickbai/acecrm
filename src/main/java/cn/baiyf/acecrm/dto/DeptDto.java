package cn.baiyf.acecrm.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class DeptDto {

    private Integer id;

    @NotEmpty(message = "请输入姓名")
    private String name;

    private Integer pid;

    @NotNull(message = "请选择状态")
    private Integer status;

    private String desc;
}
