package cn.baiyf.acecrm.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class RoleDto {

    private Integer id;

    @NotEmpty(message = "请输入角色名")
    private String name;

    @NotNull(message = "请选择状态")
    private Integer status;

    @NotEmpty(message = "请分配角色权限")
    private List<Integer> roleNode;
}
