package cn.baiyf.acecrm.dto;

import lombok.Data;

import java.util.List;

@Data
public class PageReturnDto<T> {

    private List<T> data;

    private Long total;

    private Long nowPage;

    private Long pages;
}