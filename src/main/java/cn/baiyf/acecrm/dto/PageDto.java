package cn.baiyf.acecrm.dto;

import lombok.Data;

@Data
public class PageDto {

    private Integer page;

    private Integer pageSize;
}
