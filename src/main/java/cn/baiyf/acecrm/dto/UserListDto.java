package cn.baiyf.acecrm.dto;

import lombok.Data;

@Data
public class UserListDto extends PageDto {

    private String nickname;

    private Integer deptId;

    private Integer status;
}
