package cn.baiyf.acecrm.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class AddLeadsDto {

    @NotEmpty(message = "请输入线索名称")
    private String name;

    private String companyName;

    private Integer sourceId;

    private String phone;

    private String telphone;

    private String wechat;

    private String qq;

    private String email;

    private String address;

    private Integer industry;

    private Integer level;

    private String nextContactTime;

    private String remark;
}
