package cn.baiyf.acecrm.dto;

import lombok.Data;

@Data
public class LoginReDto {

    private String token;

    private Integer roleId;

    private Integer userId;
}
