package cn.baiyf.acecrm.vo;

import lombok.Data;

@Data
public class UserListVO {

    /**
     * 管理员id
     */
    private Integer id;

    /**
     * 登录名称
     */
    private String name;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 管理员头像
     */
    private String avatar;

    /**
     * 部门id
     */
    private Integer deptId;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 所属角色id
     */
    private Integer roleId;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 1 正常 2 禁用
     */
    private Integer status;

    /**
     * 是否是领导 1:是 2:否
     */
    private Integer isLeader;

    /**
     * 直属上级id
     */
    private Integer leaderId;

    /**
     * 创建时间
     */
    private String createTime;
}
