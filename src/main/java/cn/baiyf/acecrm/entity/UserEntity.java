package cn.baiyf.acecrm.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author NickBai
 * @since 2022-08-03
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("ac_user")
public class UserEntity extends Model<UserEntity> {

    /**
     * 管理员id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 登录名称
     */
    @TableField("name")
    private String name;

    /**
     * 昵称
     */
    @TableField("nickname")
    private String nickname;

    /**
     * 管理员密码
     */
    @TableField("password")
    private String password;

    /**
     * 当前用户加密盐
     */
    @TableField("salt")
    private String salt;

    /**
     * 管理员头像
     */
    @TableField("avatar")
    private String avatar;

    /**
     * 部门id
     */
    @TableField("dept_id")
    private Integer deptId;

    /**
     * 所属角色id
     */
    @TableField("role_id")
    private Integer roleId;

    /**
     * 最近一次登录ip
     */
    @TableField("last_login_ip")
    private String lastLoginIp;

    /**
     * 最近一次登录时间
     */
    @TableField("last_login_time")
    private String lastLoginTime;

    /**
     * 最近一次登录设备
     */
    @TableField("last_login_agent")
    private String lastLoginAgent;

    /**
     * 1 正常 2 禁用
     */
    @TableField("status")
    private Integer status;

    /**
     * 是否是领导 1:是 2:否
     */
    @TableField("is_leader")
    private Integer isLeader;

    /**
     * 直属上级id
     */
    @TableField("leader_id")
    private Integer leaderId;

    /**
     * 添加时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private String createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private String updateTime;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
