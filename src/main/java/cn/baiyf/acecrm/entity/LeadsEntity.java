package cn.baiyf.acecrm.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 线索表
 * </p>
 *
 * @author NickBai
 * @since 2022-08-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("ac_leads")
public class LeadsEntity extends Model<LeadsEntity> {

    /**
     * 线索id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 线索名称
     */
    @TableField("name")
    private String name;

    /**
     * 公司名称
     */
    @TableField("company_name")
    private String companyName;

    /**
     * 线索来源
     */
    @TableField("source_id")
    private Integer sourceId;

    /**
     * 手机号
     */
    @TableField("phone")
    private String phone;

    /**
     * 电话号
     */
    @TableField("telphone")
    private String telphone;

    /**
     * 微信
     */
    @TableField("wechat")
    private String wechat;

    /**
     * qq
     */
    @TableField("qq")
    private String qq;

    /**
     * 邮箱号
     */
    @TableField("email")
    private String email;

    /**
     * 地址
     */
    @TableField("address")
    private String address;

    /**
     * 客户行业
     */
    @TableField("industry")
    private Integer industry;

    /**
     * 客户级别
     */
    @TableField("level")
    private Integer level;

    /**
     * 下次联系时间
     */
    @TableField("next_contact_time")
    private String nextContactTime;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

    /**
     * 归属人id
     */
    @TableField("owner_id")
    private Integer ownerId;

    /**
     * 归属人
     */
    @TableField("owner")
    private String owner;

    /**
     * 录入人id
     */
    @TableField("operator_id")
    private Integer operatorId;

    /**
     * 录入人名
     */
    @TableField("operator")
    private String operator;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private String createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private String updateTime;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
