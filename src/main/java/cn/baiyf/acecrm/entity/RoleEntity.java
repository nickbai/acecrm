package cn.baiyf.acecrm.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author NickBai
 * @since 2022-08-05
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("ac_role")
public class RoleEntity extends Model<RoleEntity> {

    /**
     * 角色id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 角色名称
     */
    @TableField("name")
    private String name;

    /**
     * 角色拥有的菜单节点
     */
    @TableField("role_node")
    private String roleNode;

    /**
     * 状态  1 有效 2 无效
     */
    @TableField("status")
    private Integer status;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private String createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private String updateTime;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
