package cn.baiyf.acecrm.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 部门表
 * </p>
 *
 * @author NickBai
 * @since 2022-08-05
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("ac_dept")
public class DeptEntity extends Model<DeptEntity> {

    /**
     * 部门id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 部门名称
     */
    @TableField("name")
    private String name;

    /**
     * 上级部门id
     */
    @TableField("pid")
    private Integer pid;

    /**
     * 1:启用 2:禁用
     */
    @TableField("status")
    private Integer status;

    /**
     * 部门描述
     */
    @TableField("description")
    private String description;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private String createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private String updateTime;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
