package cn.baiyf.acecrm.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class TreeNodeEntity extends NodeEntity {

    private List<TreeNodeEntity> children;
}
