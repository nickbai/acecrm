package cn.baiyf.acecrm.entity;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class TreeDeptEntity extends DeptEntity {

    private List<TreeDeptEntity> children;
}
