package cn.baiyf.acecrm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 节点表
 * </p>
 *
 * @author NickBai
 * @since 2022-08-04
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("ac_node")
public class NodeEntity extends Model<NodeEntity> {

    /**
     * 角色id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 节点名称
     */
    @TableField("node_name")
    private String nodeName;

    /**
     * 菜单标识
     */
    @TableField("flag")
    private String flag;

    /**
     * 前端显示路由
     */
    @TableField("web_path")
    private String webPath;

    /**
     * 节点路径
     */
    @TableField("node_path")
    private String nodePath;

    /**
     * 加载的模板
     */
    @TableField("component")
    private String component;

    /**
     * 所属节点
     */
    @TableField("node_pid")
    private Integer nodePid;

    /**
     * 节点图标
     */
    @TableField("node_icon")
    private String nodeIcon;

    /**
     * 是否是菜单项 1 不是 2 是
     */
    @TableField("is_menu")
    private Integer menu;

    /**
     * 排序，值越大越靠前
     */
    @TableField("sort")
    private Integer sort;

    /**
     * 添加时间
     */
    @TableField("add_time")
    private String addTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private String updateTime;

    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
