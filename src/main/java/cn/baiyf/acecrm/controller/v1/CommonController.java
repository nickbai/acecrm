package cn.baiyf.acecrm.controller.v1;

import cn.baiyf.acecrm.util.Result;
import cn.hutool.core.util.IdUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/upload")
@Slf4j
public class CommonController {

    @Value("${acecrm.upload-file-dir}")
    private String uploadRealDir;

    @PostMapping("/img")
    public Result Img(HttpServletRequest request, MultipartFile file) {

        try {
            // 保存文件
            String etc = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
            String serverPath = request.getScheme() + "://" + request.getServerName()
                    + ":" + request.getServerPort() + request.getContextPath() + "/file/img/";
            String fileName = IdUtil.simpleUUID() + "." + etc;

            // 文件保存再真实路径下
            File saveFile = new File(uploadRealDir + "img/" + fileName);
            System.out.println(saveFile);
            if (!saveFile.getParentFile().exists()) { // 目录不存在，创建目录
                saveFile.mkdirs();
            }
            file.transferTo(saveFile); // 保存上传文件

            return Result.success(serverPath + fileName);
        } catch (IOException e) {
            log.error("上传错误: {}", e.getMessage());
            return Result.fail();
        }
    }
}
