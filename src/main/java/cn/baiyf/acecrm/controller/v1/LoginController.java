package cn.baiyf.acecrm.controller.v1;

import cn.baiyf.acecrm.dto.LoginDto;
import cn.baiyf.acecrm.service.NodeService;
import cn.baiyf.acecrm.service.UserService;
import cn.baiyf.acecrm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/login")
@RestController
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private NodeService nodeService;

    @PostMapping("/index")
    public Result index(@Validated @RequestBody LoginDto loginDto) {
        return userService.doLogin(loginDto);
    }

    @GetMapping("/getMenu")
    public Result getMenu(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        return nodeService.getMenu(token);
    }
}
