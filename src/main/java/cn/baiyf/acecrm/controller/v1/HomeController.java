package cn.baiyf.acecrm.controller.v1;

import cn.baiyf.acecrm.util.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/home")
@RestController
public class HomeController {

    @GetMapping("/index")
    public Result index() {
        return Result.success();
    }
}
