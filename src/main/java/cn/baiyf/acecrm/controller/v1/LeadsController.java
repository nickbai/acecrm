package cn.baiyf.acecrm.controller.v1;

import cn.baiyf.acecrm.dto.AddLeadsDto;
import cn.baiyf.acecrm.dto.LeadsDto;
import cn.baiyf.acecrm.service.LeadsService;
import cn.baiyf.acecrm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 线索表 前端控制器
 * </p>
 *
 * @author NickBai
 * @since 2022-08-10
 */
@RestController
@RequestMapping("/leads")
public class LeadsController {

    @Autowired
    private LeadsService leadsService;

    @GetMapping("/index")
    public Result index(@RequestBody LeadsDto leadsDto) {
        return leadsService.getLeadsList(leadsDto);
    }

    @PostMapping("/add")
    public Result addLeads(@RequestBody @Validated AddLeadsDto addLeadsDto, HttpServletRequest request) {
        return leadsService.addLeads(addLeadsDto, request.getHeader("Authorization"));
    }
}
