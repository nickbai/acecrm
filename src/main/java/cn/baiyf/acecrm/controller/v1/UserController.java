package cn.baiyf.acecrm.controller.v1;

import cn.baiyf.acecrm.dto.UserAddDto;
import cn.baiyf.acecrm.dto.UserListDto;
import cn.baiyf.acecrm.service.UserService;
import cn.baiyf.acecrm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author NickBai
 * @since 2022-08-03
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/index")
    public Result index(UserListDto userListDto) {
        return userService.getUserList(userListDto);
    }

    @PostMapping("/add")
    public Result add(@Validated @RequestBody UserAddDto userAddDto) {
        return userService.addUser(userAddDto);
    }

    @PostMapping("/edit")
    public Result edit(@Validated @RequestBody UserAddDto userAddDto) {
        return userService.editUser(userAddDto);
    }

    @GetMapping("/del")
    public Result del(@RequestParam("id") Integer userId) {
        return userService.delUser(userId);
    }
}
