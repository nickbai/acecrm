package cn.baiyf.acecrm.controller.v1;

import cn.baiyf.acecrm.dto.RoleDto;
import cn.baiyf.acecrm.dto.StatusDto;
import cn.baiyf.acecrm.entity.UserEntity;
import cn.baiyf.acecrm.enums.SourceEnum;
import cn.baiyf.acecrm.service.NodeService;
import cn.baiyf.acecrm.service.RoleService;
import cn.baiyf.acecrm.service.UserService;
import cn.baiyf.acecrm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author NickBai
 * @since 2022-08-05
 */
@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private NodeService nodeService;

    @Autowired
    private UserService userService;

    @RequestMapping("/index")
    public Result index(StatusDto statusDto) {
        return roleService.getRoleList(statusDto);
    }

    @RequestMapping("/showNode")
    public Result showNode() {
        return nodeService.showNode();
    }

    @PostMapping("/add")
    public Result add(@RequestBody @Validated RoleDto roleDto) {
        return roleService.addRole(roleDto);
    }

    @PostMapping("/edit")
    public Result edit(@RequestBody @Validated RoleDto roleDto) {
        return roleService.editRole(roleDto);
    }

    @GetMapping("/del")
    public Result del(@RequestParam("id") int id) {

        Result<List<UserEntity>> res = userService.getUserListByRoleId(id);
        if (!CollectionUtils.isEmpty(res.getData())) {
            return Result.fail("该角色已被使用，无法删除。", "");
        }

        return roleService.delRole(id);
    }
}
