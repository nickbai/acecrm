package cn.baiyf.acecrm.controller.v1;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 节点表 前端控制器
 * </p>
 *
 * @author NickBai
 * @since 2022-08-04
 */
@RestController
@RequestMapping("/nodeEntity")
public class NodeController {

}
