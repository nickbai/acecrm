package cn.baiyf.acecrm.controller.v1;

import cn.baiyf.acecrm.dto.DeptDto;
import cn.baiyf.acecrm.dto.StatusDto;
import cn.baiyf.acecrm.entity.UserEntity;
import cn.baiyf.acecrm.service.DeptService;
import cn.baiyf.acecrm.service.UserService;
import cn.baiyf.acecrm.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 部门表 前端控制器
 * </p>
 *
 * @author NickBai
 * @since 2022-08-05
 */
@RestController
@RequestMapping("/dept")
public class DeptController {

    @Autowired
    private DeptService deptService;

    @Autowired
    private UserService userService;

    @GetMapping("/index")
    public Result index(StatusDto statusDto) {
        return deptService.getDeptTree(statusDto);
    }

    @PostMapping("/add")
    public Result add(@RequestBody @Validated DeptDto deptDto) {
        return deptService.addDept(deptDto);
    }

    @PostMapping("/edit")
    public Result edit(@RequestBody @Validated DeptDto deptDto) {
        return deptService.editDept(deptDto);
    }

    @GetMapping("/del")
    public Result del(@RequestParam("id") Integer id) {

        Result<List<UserEntity>> res = userService.getUserListByDeptId(id);
        if (!CollectionUtils.isEmpty(res.getData())) {
            return Result.fail("该部门下有员工，不可删除。", "");
        }

        return deptService.delDept(id);
    }
}
