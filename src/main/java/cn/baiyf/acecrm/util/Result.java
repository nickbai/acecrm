package cn.baiyf.acecrm.util;

import lombok.Data;

@Data
public class Result<T> {

    public static final Integer SUCCESS_CODE = 200;
    public static final Integer FAIL_CODE = 500;
    public static final String SUCCESS_MESSAGE = "success";
    public static final String FAIL_MESSAGE = "fail";
    /**
     * 返回状态码
     */
    private Integer code;
    /**
     * 返回信息
     */
    private String message;

    /**
     * 返回数据
     */
    private T data;

    private Result() {
    }

    public static <T> Result<T> success() {
        Result<T> resultUtil = new Result<>();
        resultUtil.setCode(SUCCESS_CODE);
        resultUtil.setMessage(SUCCESS_MESSAGE);
        return resultUtil;
    }

    public static <T> Result<T> success(T data) {
        Result<T> resultUtil = success();
        resultUtil.setData(data);
        return resultUtil;
    }

    public static <T> Result<T> success(String message, T data) {
        Result<T> resultUtil = success();
        resultUtil.setMessage(message);
        resultUtil.setData(data);
        return resultUtil;
    }

    public static <T> Result<T> success(Integer code, String message, T data) {
        Result<T> resultUtil = new Result<>();
        resultUtil.setCode(code);
        resultUtil.setMessage(message);
        resultUtil.setData(data);
        return resultUtil;
    }

    public static <T> Result<T> fail() {
        Result<T> resultUtil = new Result<>();
        resultUtil.setCode(FAIL_CODE);
        resultUtil.setMessage(FAIL_MESSAGE);
        return resultUtil;
    }

    public static <T> Result<T> fail(T data) {
        Result<T> resultUtil = fail();
        resultUtil.setData(data);
        return resultUtil;
    }

    public static <T> Result<T> fail(String message, T data) {
        Result<T> resultUtil = fail();
        resultUtil.setMessage(message);
        resultUtil.setData(data);
        return resultUtil;
    }

    public static <T> Result<T> fail(Integer code, String message) {
        Result<T> resultUtil = fail();
        resultUtil.setCode(code);
        resultUtil.setMessage(message);
        return resultUtil;
    }

    public static <T> Result<T> fail(Integer code, String message, T data) {
        Result<T> resultUtil = new Result<>();
        resultUtil.setCode(code);
        resultUtil.setMessage(message);
        resultUtil.setData(data);
        return resultUtil;
    }
}
