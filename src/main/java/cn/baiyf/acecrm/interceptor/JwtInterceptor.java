package cn.baiyf.acecrm.interceptor;

import cn.baiyf.acecrm.util.JwtUtil;
import com.alibaba.fastjson.JSON;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class JwtInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("Authorization");
        Map<String, Object> map = new HashMap<>();
        map.put("code", 403);
        map.put("data", "");

        if (token != null) {
            try {
                JwtUtil.verify(token);
                return true;
            } catch (Exception e) {
                map.put("message", "token无效");
            }
        } else {
            map.put("message", "token为空");
        }

        // 错误信息响应到前台
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().print(JSON.toJSON(map));
        response.setStatus(HttpStatus.OK.value());
        return false;
    }
}